from django.db import models


class Product(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    stock = models.BooleanField(False)
    price = models.DecimalField(max_digits=9, decimal_places=2)
    links = models.TextField()

    def __str___(self):
        return self.title
