from rest_framework.response import Response
from rest_framework.decorators import api_view
from scraper.main import scrape


@api_view(['GET'])
def product_view(request):

    if request.method == 'GET':
        url = request.META['HTTP_URL_TO_SCRAPE']
        if url:
            data = scrape(url=url)

        return Response({
            'success': True,
            'meesage': 'Get Request Fulfilled!',
            'data': data})

    if request.method == 'POST':
        print(request.headers.keys())
        return Response({
            'success': True,
            'meesage': 'Post Request Fulfilled!',
            'data': ''
        })

    return Response({
        'success': False,
        'meesage': 'Invalid Request',
        'data': ''
    })
