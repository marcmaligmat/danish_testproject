from django.urls import path
from api.views.fn_based import product_view

urlpatterns = [
    path("scrape/", product_view),
]
